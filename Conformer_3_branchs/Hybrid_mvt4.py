# -*- coding: utf-8 -*-
import math
import logging
from functools import partial
from collections import OrderedDict
from modeling_resnet import ResNetV2

import torch
import torch.nn as nn
import torch.nn.functional as F

import timm
from timm.models.layers import DropPath, to_2tuple, trunc_normal_  


from utils_mvt import get_masked_tensor_api #这个是自己额外写的函数

class Mlp(nn.Module):
    def __init__(self, in_features, hidden_features=None, out_features=None, act_layer=nn.GELU, drop=0.):
        super().__init__()
        out_features = out_features or in_features
        hidden_features = hidden_features or in_features
        self.fc1 = nn.Linear(in_features, hidden_features)
        self.act = act_layer()
        self.fc2 = nn.Linear(hidden_features, out_features)
        self.drop = nn.Dropout(drop)

    def forward(self, x):
        x = self.fc1(x)
        x = self.act(x)
        x = self.drop(x)
        x = self.fc2(x)
        x = self.drop(x)
        return x
        x = self.proj_drop(x)
        
        # x: (B, N, C)
        return x

class Attention(nn.Module):
    def __init__(self, dim, num_heads=8, qkv_bias=False, qk_scale=None, attn_drop=0., proj_drop=0.):
        super().__init__()
        self.num_heads = num_heads
        head_dim = dim // num_heads
        # NOTE scale factor was wrong in my original version, can set manually to be compat with prev weights
        self.scale = qk_scale or head_dim ** -0.5

        self.qkv = nn.Linear(dim, dim * 3, bias=qkv_bias)
        self.attn_drop = nn.Dropout(attn_drop)
        self.proj = nn.Linear(dim, dim)
        self.proj_drop = nn.Dropout(proj_drop)

    def forward(self, x): # 输入是按照 B N C 的格式 输出还是 B N C的格式 是不变的 
        B, N, C = x.shape  
        qkv = self.qkv(x).reshape(B, N, 3, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)
        q, k, v = qkv[0], qkv[1], qkv[2]   # make torchscript happy (cannot use tensor as tuple)

        attn = (q @ k.transpose(-2, -1)) * self.scale
        attn = attn.softmax(dim=-1)
        attn = self.attn_drop(attn)

        x = (attn @ v).transpose(1, 2).reshape(B, N, C)
        x = self.proj(x)
        return x

class PatchEmbed(nn.Module):
    """ Image to Patch Embedding
    """
    def __init__(self, img_size=224, patch_size=16, in_chans=3, embed_dim=768):
        super().__init__()
        img_size = (img_size,img_size)  # to_2tuple 函数 是timm模块自带的
        patch_size = (patch_size,patch_size)
        num_patches = (img_size[1] // patch_size[1]) * (img_size[0] // patch_size[0])
        self.img_size = img_size
        self.patch_size = patch_size
        self.num_patches = num_patches

        self.proj = nn.Conv2d(in_chans, embed_dim, kernel_size=patch_size, stride=patch_size)

    def forward(self, x):
        B, C, H, W = x.shape
        # FIXME look at relaxing size constraints
        assert H == self.img_size[0] and W == self.img_size[1], \
            f"Input image size ({H}*{W}) doesn't match model ({self.img_size[0]}*{self.img_size[1]})."
        x = self.proj(x).flatten(2).transpose(1, 2)

        # x: (B, 14*14, 768)
        return x    

class Block_original(nn.Module): #最开始Block的定义

    def __init__(self, dim, num_heads, mlp_ratio=4., qkv_bias=False, qk_scale=None, drop=0., attn_drop=0.,
                 drop_path=0., act_layer=nn.GELU, norm_layer=nn.LayerNorm):
        super().__init__()
        self.norm1 = norm_layer(dim)
        self.attn = Attention(
            dim, num_heads=num_heads, qkv_bias=qkv_bias, qk_scale=qk_scale, attn_drop=attn_drop, proj_drop=drop)
        # NOTE: drop path for stochastic depth, we shall see if this is better than dropout here
        self.drop_path = DropPath(drop_path) if drop_path > 0. else nn.Identity()
        self.norm2 = norm_layer(dim)
        mlp_hidden_dim = int(dim * mlp_ratio)
        self.mlp = Mlp(in_features=dim, hidden_features=mlp_hidden_dim, act_layer=act_layer, drop=drop)

    def forward(self, x):
        x = x + self.drop_path(self.attn(self.norm1(x)))
        x = x + self.drop_path(self.mlp(self.norm2(x)))
        return x

class Block(nn.Module):
    #重复4次的推断
    def __init__(self, dim, num_heads, dim_mask, dim_por, mlp_ratio=4., qkv_bias=False, qk_scale=None, drop=0., attn_drop=0.,
                 drop_path=0., act_layer=nn.GELU, norm_layer=nn.LayerNorm):
        super().__init__()
        self.norm1 = norm_layer(dim)
        self.attn = Attention(
            dim, num_heads=num_heads, qkv_bias=qkv_bias, qk_scale=qk_scale, attn_drop=attn_drop, proj_drop=drop)
        # NOTE: drop path for stochastic depth, we shall see if this is better than dropout here
        self.drop_path = DropPath(drop_path) if drop_path > 0. else nn.Identity()
        self.norm2 = norm_layer(dim)
        mlp_hidden_dim = int(dim * mlp_ratio)
        self.norm3 = norm_layer(int(dim*dim_por))
        self.mlp = Mlp(in_features=dim, hidden_features=mlp_hidden_dim, act_layer=act_layer, drop=drop)

        self.dim_mask = dim_mask
        self.dim_por = dim_por
        self.head = Mlp(in_features=int(dim*self.dim_por), hidden_features=int(dim*dim_por/2), out_features=dim,act_layer=act_layer,drop=drop)
    def forward(self, x, train):
        if train:
            tensor_list,y = get_masked_tensor_api(x, dim_mask=self.dim_mask, dim_por=self.dim_por)
            for x in tensor_list: # 这一步把list中的tensor遍历了  这么写现在看不是特别优雅
                x = x + self.drop_path(self.attn(self.norm1(x)))            
                x = x + self.drop_path(self.mlp(self.norm2(x)))
                y = torch.cat((y,x),2)
        else:
            x = x + self.drop_path(self.attn(self.norm1(x)))            
            x = x + self.drop_path(self.mlp(self.norm2(x)))
            y = x.repeat([1,1,4])
        y = self.head(self.norm3(y))
        return y




class HybridEmbed(nn.Module): #目前这个函数没有用上 只是为了不大改 Vision Transformer源代码
    """ CNN Feature Map Embedding
    Extract feature map from CNN, flatten, project to embedding dim.
    """
    def __init__(self, backbone, img_size=224, feature_size=None, in_chans=3, embed_dim=768):
        super().__init__()
        assert isinstance(backbone, nn.Module)
        img_size = to_2tuple(img_size)
        self.img_size = img_size
        self.backbone = backbone
        if feature_size is None:
            with torch.no_grad():
                # FIXME this is hacky, but most reliable way of determining the exact dim of the output feature
                # map for all networks, the feature metadata has reliable channel and stride info, but using
                # stride to calc feature dim requires info about padding of each stage that isn't captured.
                training = backbone.training
                if training:
                    backbone.eval()
                o = self.backbone(torch.zeros(1, in_chans, img_size[0], img_size[1]))
                if isinstance(o, (list, tuple)):
                    o = o[-1]  # last feature if backbone outputs list/tuple of features
                feature_size = o.shape[-2:]
                feature_dim = o.shape[1]
                backbone.train(training)
        else:
            feature_size = to_2tuple(feature_size)
            if hasattr(self.backbone, 'feature_info'):
                feature_dim = self.backbone.feature_info.channels()[-1]
            else:
                feature_dim = self.backbone.num_features
        self.num_patches = feature_size[0] * feature_size[1]
        self.proj = nn.Conv2d(feature_dim, embed_dim, 1)#1024→768
        
    def forward(self, x):
        x = self.backbone(x)
        if isinstance(x, (list, tuple)):
            x = x[-1]  # last feature if backbone outputs list/tuple of features
        x = self.proj(x).flatten(2).transpose(1, 2)
        return x

class MaskTransformer(nn.Module):
    """ Vision Transformer with support for patch or hybrid CNN input stage
    
    dim_por 是新增的变量 代表在mask_block中的宽度
    dim_mask 数值上等同于patchsize
    patchsize的意思是每个patch的大小
    224//16
    
    """
    def __init__(self, img_size=224, patch_size=16, in_chans=3, num_classes=1000, embed_dim=768, depth_mask=1, depth_norm=2,
                 dim_por=4, num_heads=12, mlp_ratio=4., qkv_bias=True, qk_scale=None, drop_rate=0., attn_drop_rate=0.,
                 drop_path_rate=0., hybrid_backbone=None, norm_layer=nn.LayerNorm,representation_size=None):
        super().__init__()
        self.num_classes = num_classes
        self.num_features = self.embed_dim = embed_dim  # num_features for consistency with other models
        norm_layer = norm_layer or partial(nn.LayerNorm, eps=1e-6)
        self.dim_por = dim_por
        
        if hybrid_backbone is not None:
            self.patch_embed = HybridEmbed(
                hybrid_backbone, img_size=img_size, in_chans=in_chans, embed_dim=embed_dim)
        else:
            self.patch_embed = PatchEmbed(
                img_size=img_size, patch_size=patch_size, in_chans=in_chans, embed_dim=embed_dim)
        num_patches = self.patch_embed.num_patches

        self.cls_token = nn.Parameter(torch.zeros(1, 1, embed_dim))
        self.pos_embed = nn.Parameter(torch.zeros(1, num_patches + 1, embed_dim))
        self.pos_drop = nn.Dropout(p=drop_rate)

        self.dim_mask = int(img_size//patch_size) #额外定义的变量
        
        
        self.dpr = [x.item() for x in torch.linspace(0, drop_path_rate, depth_mask)]  # stochastic depth decay rule
        self.dpr1 = [x.item() for x in torch.linspace(0, drop_path_rate, depth_norm)]  # 对于ViT的block，需要另外设置模块和深度
        
        self.blocks = nn.ModuleList([
            Block(  #正常的Vision Transformer没有问题 这里只改这一个block
                dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,dim_mask=self.dim_mask,dim_por=self.dim_por,
                drop=drop_rate, attn_drop=attn_drop_rate, drop_path=self.dpr[i], norm_layer=norm_layer,
            )
            for i in range(depth_mask)])
        
                
        self.vit_blocks = nn.ModuleList([
            Block_original( 
                dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                drop=drop_rate, attn_drop=attn_drop_rate, drop_path=self.dpr1[j], norm_layer=norm_layer,
            )
            for j in range(depth_norm)])
        
        self.norm = norm_layer(embed_dim)

        # NOTE as per official impl, we could have a pre-logits representation dense layer + tanh here
        #self.repr = nn.Linear(embed_dim, representation_size)
        #self.repr_act = nn.Tanh()

        # Classifier head
        if representation_size is not None:
            self.head = nn.Sequential(nn.Linear(embed_dim, representation_size),
                        nn.Tanh(),
                        nn.Linear(representation_size,num_classes),
                        ) if num_classes > 0 else nn.Identity()
        else:
            self.head = nn.Linear(embed_dim, num_classes) if num_classes > 0 else nn.Identity()

        trunc_normal_(self.pos_embed, std=.02)
        trunc_normal_(self.cls_token, std=.02)
        self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, nn.Linear):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, nn.Linear) and m.bias is not None:
                nn.init.constant_(m.bias, 0)
        elif isinstance(m, nn.LayerNorm):
            nn.init.constant_(m.bias, 0)
            nn.init.constant_(m.weight, 1.0)

    @torch.jit.ignore
    def no_weight_decay(self):
        return {'pos_embed', 'cls_token'}

    def get_classifier(self):
        return self.head

    def reset_classifier(self, num_classes, global_pool=''):
        self.num_classes = num_classes
        self.head = nn.Linear(self.embed_dim, num_classes) if num_classes > 0 else nn.Identity()

    def forward_features(self, x, train):
        B = x.shape[0]
        #[B, 3, 224, 224]
        x = self.patch_embed(x)
        #[B, 196, 768]
        
#         print('patch的维度',x.shape)
        cls_tokens = self.cls_token.expand(B, -1, -1)  # stole cls_tokens impl from Phil Wang, thanks
        [8, 1, 768]
        x = torch.cat((cls_tokens, x), dim=1)
        [8, 197, 768]
#         print(self.pos_embed.shape)
        x = x + self.pos_embed
        x = self.pos_drop(x)
#         print('输入mask的维度',x.shape)
        for blk1 in self.blocks:
            x = blk1(x, train)
#         print('经过注意力模块后的维度',x.shape)
        for blk2 in self.vit_blocks:
            x = blk2(x)
        x = self.norm(x)
#         print('用于预测的cls_token维度',x[:, 0].shape)
        return x[:, 0]

    def forward(self, x, train):
#         print('输入维度',x.shape)
        x_t = self.forward_features(x, train)
        x_cls = self.head(x_t)
        return x_t, x_cls
