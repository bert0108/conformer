# -*- coding: utf-8 -*-
import random   
import torch
#dim_mask = 8 #代表我希望把原始的patch set 切分为多少块 如 4*4的patch就是切分为4块 这里就为4 16*16就是16块
#dim_por = 4 #这里代表着比例 比如 dim_mask为16 那我需要其中4个为一组 dim_portion定义了一共有多少组 此处为4代表 4个mask分成四个组

def random_split(mask_list,dim_mask,dim_por): #随机平均切分list的函数 被generate_random函数调用
    random.shuffle(mask_list)
    # 此处应当后续完善加一个警告，不能输入无法整除的变量
    por = dim_mask//dim_por
    mask_split = []
    for i in range(dim_por):
        mask_split.append(mask_list[por*i:por*i+por])
    return mask_split

def generate_random_position_mask(dim_mask,dim_por): # 生成随机的position mask  该生成过程并不包含class token的定义
    mask_list = [i for i in range(1,
                                  +1)]
    mask_all = []
    
    from collections import deque
    random.shuffle(mask_list)
    mask_deque = deque(mask_list)
    mask_deque_copy = mask_deque.copy()

    for i in range(0,dim_mask):    
        mask_deque_copy.rotate(1)
        mask_all.append(mask_deque_copy.copy()) #这里完成了平移 本质上实现了对原始patch的位置编码

    random.shuffle(mask_all) #行交换
    mask_embed = []
    for deque in mask_all:   #将所有deque合并为一个list
        mask_embed += list(deque)
    #print(mask_embed)   #这里用1 2 3 4 。。。 代表不同的mask
    
    mask_split = random_split(mask_list,dim_mask,dim_por) #这一步返回若干个包含分割后结果的    
    #print(mask_split)
    mask_embed_all = [] #存储最终输出的各个mask
     
    
    for mask_number in mask_split: #这里实现了 我分成了几组  就对应添加几个mask
        cur_mask_embed = []
        cur_mask_embed.append(0) #代表对于class token  一定保留 
        for i in mask_embed:
            if i in mask_number:
                cur_mask_embed.append(1)
            else: 
                cur_mask_embed.append(0)
                      
        mask_embed_all.append(cur_mask_embed)
        
    return mask_embed_all #此处返回一个list in list list中每一个元素为一个mask_list

def embed_mask(embed,mask_list): #输入的[B N D] mask_list 为对应N维的
    embed = embed.transpose(1,2)
    # 注 这一步默认为需要gpu 当前不支持任意切换 后续完善
    mask = torch.tensor(mask_list).bool().cuda()  #在进行masked操作的时候 是按照最底层的那一维进行的 1代表被mask 0 代表保留
    embed = embed.masked_fill(mask,value = 0.0).transpose(1,2)
    return embed


# 该函数集成了 generate_random embed_mask 函数的功能， 直接返回经过mask操作的张量
def get_masked_tensor_api(embed,dim_mask,dim_por):
    mask_all = generate_random_position_mask(dim_mask=dim_mask,dim_por=dim_por)
    masked_tensor = []
    y = torch.tensor([]).cuda()
    for mask in mask_all:
        embed_i = embed.clone()
        masked_tensor.append(embed_mask(embed,mask))
    del mask_all
    return masked_tensor, y
