
OUTPUT='./output-base'
# export CUDA_VISIBLE_DEVICES=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15

# python -m torch.distributed.launch --master_port 13 --nproc_per_node 16 --use_env train_bigmodel.py \
#                 --model Conformer_base_patch16  \
#                 --in-feature 2880 \
#                 --depth-mask 2 \
#                 --depth-norm 2 \
#                 --batch-size 64 \
#                 --lr  1e-5 \
#                 --input-size 224 \
#                 --data-set IMNET \
#                 --num_workers 16 \
#                 --data-path /userhome/data/imagenet \
#                 --output_dir ${OUTPUT}\
#                 --clip-grad 0.5 
#                 --resume ./output-0803/checkpoint.pth
                #deit_test1/jiaojiayu/conformer101/output-0803_1/checkpoint.pth
                #deit_test1/jiaojiayu/conformer101/mask_checkpoint/Conformer_base_patch16.pth

                # --sync-bn \
                # --resume output/best_model.pth \
                # --dist-eval \
                # --eval

python train_bigmodel.py \
                      --model Conformer_base_patch16 \
                      --in-feature 2880 \
                      --depth-mask 2 \
                      --depth-norm 2 \
                      --batch-size 8 \
                      --lr 1e-5 \
                      --input-size 224 \
                      --data-set IMNET \
                      --data-path /data/pytorch-imagenet-data \
                      --output_dir ${OUTPUT} \
                      --clip-grad 0.5