# -*- coding: utf-8 -*-
import math
import logging
from functools import partial
from collections import OrderedDict
from modeling_resnet import ResNetV2

# import torch
# import torch.nn as nn
# import torch.nn.functional as F

import mindspore
import mindspore.nn as nn
import mindspore.ops.functional as F
import mindspore.common.initializer as Init
from mindspore import Tensor
from mindspore.ops import operations as P

# import timm
import msimm
from msimm import DropPath, trunc_normal_  
from timm.models.layers import to_2tuple


from utils_mvt import get_masked_tensor_api #这个是自己额外写的函数

class Mlp(nn.Cell):
    def __init__(self, in_features, hidden_features=None, out_features=None, act_layer=nn.GELU, drop=0.):
        super().__init__()
        out_features = out_features or in_features
        hidden_features = hidden_features or in_features
        self.fc1 = nn.Dense(in_features, hidden_features)
        self.act = act_layer()
        self.fc2 = nn.Dense(hidden_features, out_features)
        self.drop = nn.Dropout(1.0-drop)

    def construct(self, x):
        x = self.fc1(x)
        x = self.act(x)
        x = self.drop(x)
        x = self.fc2(x)
        x = self.drop(x)
        return x
        x = self.proj_drop(x)
        
        # x: (B, N, C)
        return x

# class Attention(nn.Cell):
#     def __init__(self, dim, num_heads=8, qkv_bias=False, qk_scale=None, attn_drop=0., proj_drop=0.):
#         super().__init__()
#         self.num_heads = num_heads
#         head_dim = dim // num_heads
#         # NOTE scale factor was wrong in my original version, can set manually to be compat with prev weights
#         self.scale = qk_scale or head_dim ** -0.5

#         self.qkv = nn.Dense(dim, dim * 3, has_bias=qkv_bias)
#         self.attn_drop = nn.Dropout(1.0-attn_drop)
#         self.proj = nn.Dense(dim, dim)
#         self.proj_drop = nn.Dropout(1.0-proj_drop)

#     def construct(self, x): # 输入是按照 B N C 的格式 输出还是 B N C的格式 是不变的 
#         B, N, C = x.shape  
#         qkv = self.qkv(x).reshape(B, N, 3, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)
#         q, k, v = qkv[0], qkv[1], qkv[2]   # make torchscript happy (cannot use tensor as tuple)

#         attn = (q @ k.transpose(-2, -1)) * self.scale
#         attn = attn.softmax(dim=-1)
#         attn = self.attn_drop(attn)

#         x = (attn @ v).transpose((0, 2, 1)).reshape(B, N, C)
#         x = self.proj(x)
#         return x

class Attention(nn.Cell):
    """Multi-head Attention"""

    def __init__(self, dim, hidden_dim=None, num_heads=8, qkv_bias=False, qk_scale=None, attn_drop=0., proj_drop=0.):
        super(Attention, self).__init__()
        hidden_dim = hidden_dim or dim
        self.hidden_dim = hidden_dim
        self.num_heads = num_heads
        head_dim = hidden_dim // num_heads
        self.head_dim = head_dim
        self.scale = head_dim ** -0.5

        self.qk = nn.Dense(dim, hidden_dim * 2, has_bias=qkv_bias)
        self.v = nn.Dense(dim, hidden_dim, has_bias=qkv_bias)
        self.softmax = nn.Softmax(axis=-1)
        self.batmatmul_trans_b = P.BatchMatMul(transpose_b=True)
        self.attn_drop = nn.Dropout(1. - attn_drop)
        self.batmatmul = P.BatchMatMul()
        self.proj = nn.Dense(hidden_dim, dim)
        self.proj_drop = nn.Dropout(1. - proj_drop)

        self.transpose = P.Transpose()
        self.reshape = P.Reshape()

    def construct(self, x):
        """Multi-head Attention"""
        B, N, _ = x.shape
        qk = self.transpose(self.reshape(self.qk(x), (B, N, 2, self.num_heads, self.head_dim)), (2, 0, 3, 1, 4))
        q, k = qk[0], qk[1]
        v = self.transpose(self.reshape(self.v(x), (B, N, self.num_heads, self.head_dim)), (0, 2, 1, 3))

        attn = self.softmax(self.batmatmul_trans_b(q, k) * self.scale)
        attn = self.attn_drop(attn)
        x = self.reshape(self.transpose(self.batmatmul(attn, v), (0, 2, 1, 3)), (B, N, -1))
        x = self.proj(x)
        x = self.proj_drop(x)
        return x

class PatchEmbed(nn.Cell):
    """ Image to Patch Embedding
    """
    def __init__(self, img_size=224, patch_size=16, in_chans=3, embed_dim=768):
        super().__init__()
        img_size = (img_size,img_size)  # to_2tuple 函数 是timm模块自带的
        patch_size = (patch_size,patch_size)
        num_patches = (img_size[1] // patch_size[1]) * (img_size[0] // patch_size[0])
        self.img_size = img_size
        self.patch_size = patch_size
        self.num_patches = num_patches

        self.proj = nn.Conv2d(in_chans, embed_dim, kernel_size=patch_size, stride=patch_size, has_bias=True, pad_mode="pad")

    def construct(self, x):
        B, C, H, W = x.shape
        # FIXME look at relaxing size constraints
        assert H == self.img_size[0] and W == self.img_size[1], \
            f"Input image size ({H}*{W}) doesn't match model ({self.img_size[0]}*{self.img_size[1]})."
        tmp = self.proj(x)
        tmp1 = msimm.flatten(tmp, 2)
        x = tmp1.transpose((0, 2, 1))

        # x: (B, 14*14, 768)
        return x    

class Block_original(nn.Cell): #最开始Block的定义

    def __init__(self, dim, num_heads, mlp_ratio=4., qkv_bias=False, qk_scale=None, drop=0., attn_drop=0.,
                 drop_path=0., act_layer=nn.GELU, norm_layer=nn.LayerNorm):
        super().__init__()
        self.norm1 = norm_layer([dim], epsilon=1e-05)
        self.attn = Attention(
            dim, num_heads=num_heads, qkv_bias=qkv_bias, qk_scale=qk_scale, attn_drop=attn_drop, proj_drop=drop)
        # NOTE: drop path for stochastic depth, we shall see if this is better than dropout here
        self.drop_path = DropPath(drop_path) if drop_path > 0. else P.Identity()
        self.norm2 = norm_layer([dim], epsilon=1e-05)
        mlp_hidden_dim = int(dim * mlp_ratio)
        self.mlp = Mlp(in_features=dim, hidden_features=mlp_hidden_dim, act_layer=act_layer, drop=drop)

    def construct(self, x):
        x = x + self.drop_path(self.attn(self.norm1(x)))
        x = x + self.drop_path(self.mlp(self.norm2(x)))
        return x

class Block(nn.Cell):
    #重复4次的推断
    def __init__(self, dim, num_heads, dim_mask, dim_por, mlp_ratio=4., qkv_bias=False, qk_scale=None, drop=0., attn_drop=0.,
                 drop_path=0., act_layer=nn.GELU, norm_layer=nn.LayerNorm): # default epsilon of torch.nn.LayerNorm is 1e-05
        super().__init__()
        self.norm1 = norm_layer([dim], epsilon=1e-05)
        self.attn = Attention(
            dim, num_heads=num_heads, qkv_bias=qkv_bias, qk_scale=qk_scale, attn_drop=attn_drop, proj_drop=drop)
        # NOTE: drop path for stochastic depth, we shall see if this is better than dropout here
        self.drop_path = DropPath(drop_path) if drop_path > 0. else P.Identity()
        self.norm2 = norm_layer([dim], epsilon=1e-05)
        mlp_hidden_dim = int(dim * mlp_ratio)
        self.norm3 = norm_layer([int(dim*dim_por)], epsilon=1e-05)
        self.mlp = Mlp(in_features=dim, hidden_features=mlp_hidden_dim, act_layer=act_layer, drop=drop)

        self.dim_mask = dim_mask
        self.dim_por = dim_por
        self.head = Mlp(in_features=int(dim*self.dim_por), hidden_features=int(dim*dim_por/2), out_features=dim,act_layer=act_layer,drop=drop)

        self.concat = P.Concat(axis=2)
        self.repeat = P.Tile()

    def construct(self, x, train):
        if train:
            tensor_list,y = get_masked_tensor_api(x, dim_mask=self.dim_mask, dim_por=self.dim_por)
            y = tensor_list[0]
            for i in range(1, len(tensor_list)):
                x = tensor_list[i]
                x = x + self.drop_path(self.attn(self.norm1(x)))            
                x = x + self.drop_path(self.mlp(self.norm2(x)))
                y = self.concat([y,x])
        else:
            x = x + self.drop_path(self.attn(self.norm1(x)))            
            x = x + self.drop_path(self.mlp(self.norm2(x)))
            # y = x.repeat([1,1,4])
            y = self.repeat(x, [1,1,4])
        y = self.head(self.norm3(y))
        return y




class HybridEmbed(nn.Cell): #目前这个函数没有用上 只是为了不大改 Vision Transformer源代码
    """ CNN Feature Map Embedding
    Extract feature map from CNN, flatten, project to embedding dim.
    """
    def __init__(self, backbone, img_size=224, feature_size=None, in_chans=3, embed_dim=768):
        super().__init__()
        assert isinstance(backbone, nn.Cell)
        img_size = to_2tuple(img_size)
        self.img_size = img_size
        self.backbone = backbone
        self.zeros = P.Zeros()
        if feature_size is None:
            # with torch.no_grad(): # comment directly correct or not
                # FIXME this is hacky, but most reliable way of determining the exact dim of the output feature
                # map for all networks, the feature metadata has reliable channel and stride info, but using
                # stride to calc feature dim requires info about padding of each stage that isn't captured.
            training = backbone.training
            # if training:
            #     backbone.set_train(False) # backbone.eval() --> backbone.set_train(False)
            # o = self.backbone(self.zeros((1, in_chans, img_size[0], img_size[1]), mindspore.float32))
            # if isinstance(o, (list, tuple)):
            #     o = o[-1]  # last feature if backbone outputs list/tuple of features
            # feature_size = o.shape[-2:]
            # feature_dim = o.shape[1]
            feature_size = (14,14)
            feature_dim = 1024
            # print("in hybridembed:", feature_size, feature_dim)
            backbone.set_train(training)
        else:
            feature_size = to_2tuple(feature_size)
            if hasattr(self.backbone, 'feature_info'):
                feature_dim = self.backbone.feature_info.channels()[-1]
            else:
                feature_dim = self.backbone.num_features
        self.num_patches = feature_size[0] * feature_size[1]
        self.proj = nn.Conv2d(feature_dim, embed_dim, 1, has_bias=True, pad_mode="pad")#1024→768
        
    def construct(self, x):
        x = self.backbone(x)
        if isinstance(x, (list, tuple)):
            x = x[-1]  # last feature if backbone outputs list/tuple of features
        tmp = self.proj(x)
        tmp1 = msimm.flatten(tmp, 2)
        x = tmp1.transpose((0, 2, 1))
        return x

class MaskTransformer(nn.Cell):
    """ Vision Transformer with support for patch or hybrid CNN input stage
    
    dim_por 是新增的变量 代表在mask_block中的宽度
    dim_mask 数值上等同于patchsize
    patchsize的意思是每个patch的大小
    224//16
    
    """
    def __init__(self, img_size=224, patch_size=16, in_chans=3, num_classes=1000, embed_dim=768, depth_mask=1, depth_norm=2,
                 dim_por=4, num_heads=12, mlp_ratio=4., qkv_bias=True, qk_scale=None, drop_rate=0., attn_drop_rate=0.,
                 drop_path_rate=0., hybrid_backbone=None, norm_layer=nn.LayerNorm,representation_size=None, is_train=True):
        super().__init__()
        self.num_classes = num_classes
        self.num_features = self.embed_dim = embed_dim  # num_features for consistency with other models
        norm_layer = norm_layer or partial(nn.LayerNorm, epsilon=1e-6)
        self.dim_por = dim_por
        
        if hybrid_backbone is not None:
            self.patch_embed = HybridEmbed(
                hybrid_backbone, img_size=img_size, in_chans=in_chans, embed_dim=embed_dim)
        else:
            self.patch_embed = PatchEmbed(
                img_size=img_size, patch_size=patch_size, in_chans=in_chans, embed_dim=embed_dim)
        num_patches = self.patch_embed.num_patches

        self.zeros = P.Zeros()
        self.linspace = P.LinSpace()
        # concat of mindspore
        self.concat = P.Concat(axis=1)

        self.is_train = is_train

        self.cls_token = mindspore.Parameter(self.zeros((1, 1, embed_dim), mindspore.float32))
        self.pos_embed = mindspore.Parameter(self.zeros((1, num_patches + 1, embed_dim), mindspore.float32))
        self.pos_drop = nn.Dropout(keep_prob=1.0-drop_rate)

        self.dim_mask = int(img_size//patch_size) #额外定义的变量
        
        
        self.dpr = [x for x in self.linspace(Tensor(0, mindspore.float32), Tensor(drop_path_rate, mindspore.float32), depth_mask)]  # stochastic depth decay rule
        self.dpr1 = [x for x in self.linspace(Tensor(0, mindspore.float32), Tensor(drop_path_rate, mindspore.float32), depth_norm)]  # 对于ViT的block，需要另外设置模块和深度
        
        self.blocks = nn.CellList([
            Block(  #正常的Vision Transformer没有问题 这里只改这一个block
                dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,dim_mask=self.dim_mask,dim_por=self.dim_por,
                drop=drop_rate, attn_drop=attn_drop_rate, drop_path=self.dpr[i], norm_layer=norm_layer,
            )
            for i in range(depth_mask)])
        
                
        self.vit_blocks = nn.CellList([
            Block_original( 
                dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                drop=drop_rate, attn_drop=attn_drop_rate, drop_path=self.dpr1[j], norm_layer=norm_layer,
            )
            for j in range(depth_norm)])
        
        self.norm = norm_layer([embed_dim])

        # NOTE as per official impl, we could have a pre-logits representation dense layer + tanh here
        #self.repr = nn.Dense(embed_dim, representation_size)
        #self.repr_act = nn.Tanh()

        # Classifier head
        if representation_size is not None:
            self.head = nn.Sequential([nn.Dense(embed_dim, representation_size),
                        nn.Tanh(),
                        nn.Dense(representation_size,num_classes),
                        ]) if num_classes > 0 else P.Identity()
        else:
            self.head = nn.Dense(embed_dim, num_classes) if num_classes > 0 else P.Identity()

        trunc_normal_(self.pos_embed, std=.02)
        trunc_normal_(self.cls_token, std=.02)
        # self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, nn.Dense):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, nn.Dense) and m.bias is not None:
                constant_bias = Init.Constant(0.0)
                constant_bias(m.bias)
        elif isinstance(m, nn.LayerNorm):
            constant_bias = Init.Constant(0.0)
            constant_weight = Init.Constant(1.0)
            constant_bias(m.bias)
            constant_weight(m.weight)

    # @torch.jit.ignore
    # def no_weight_decay(self):
    #     return {'pos_embed', 'cls_token'}

    def get_classifier(self):
        return self.head

    def reset_classifier(self, num_classes, global_pool=''):
        self.num_classes = num_classes
        self.head = nn.Dense(self.embed_dim, num_classes) if num_classes > 0 else P.Identity()

    def forward_features(self, x, train):
        B = x.shape[0]
        x = self.patch_embed(x)
        # cls_tokens = self.cls_token.expand(B, -1, -1)  # stole cls_tokens impl from Phil Wang, thanks
        broadcastto = P.BroadcastTo((B, -1, -1))
        cls_tokens = broadcastto(self.cls_token)
        x = self.concat([cls_tokens, x])
        x = x + self.pos_embed
        x = self.pos_drop(x)
        for blk1 in self.blocks:
            x = blk1(x, train)
        for blk2 in self.vit_blocks:
            x = blk2(x)
        x = self.norm(x)
        return x[:, 0]

    def construct(self, x):
        x_t = self.forward_features(x, self.is_train)
        x_cls = self.head(x_t)
        return x_t, x_cls
