# Copyright 2021 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
""" CrossEntropySmooth.py """
import mindspore.nn as nn
from mindspore import Tensor
from mindspore.common import dtype as mstype
from mindspore.nn.loss.loss import LossBase
from mindspore.ops import functional as F
from mindspore.ops import operations as P


class CrossEntropySmooth(LossBase):
    """CrossEntropy"""
    def __init__(self, sparse=True, reduction='mean', smooth_factor=0., num_classes=1000):
        super(CrossEntropySmooth, self).__init__()
        self.onehot = P.OneHot()
        self.cast = P.Cast()
        self.sparse = sparse
        self.on_value = Tensor(1.0 - smooth_factor, mstype.float32)
        self.off_value = Tensor(1.0 * smooth_factor / (num_classes - 1), mstype.float32)
        self.ce = nn.SoftmaxCrossEntropyWithLogits(reduction=reduction)

    def construct(self, logit, label):

        if self.sparse:
            label = self.cast(label, mstype.int32)
            label = self.onehot(label, F.shape(logit[0])[1], self.on_value, self.off_value)
        if isinstance(logit, list):
            loss_list = list()
            for o in logit:
                loss = self.ce(o, label) / len(logit)
                loss_list.append(loss)
            # loss_list = [self.ce(o, label) / len(logit) for o in logit]
            loss = loss_list[0]
            for i in range(1, len(loss_list)):
                loss = loss + loss_list[i]
            # loss = sum(loss_list) # sum is not supported in graph mode
        else:
            loss = self.ce(logit, label)
        # loss = self.ce(logit, label)
        # print("in cross loss:", loss.dtype, loss.shape, loss)
        return loss
