# import torch
# import torch.nn as nn
# import torch.nn.functional as F
from functools import partial

import mindspore
import mindspore.nn as nn
import mindspore.ops.functional as F
import mindspore.common.initializer as Init
from mindspore import Tensor
from mindspore.ops import operations as P



from msimm import DropPath, trunc_normal_
import msimm


class Mlp(nn.Cell):
    def __init__(self, in_features, hidden_features=None, out_features=None, act_layer=nn.GELU, drop=0.):
        super().__init__()
        out_features = out_features or in_features
        hidden_features = hidden_features or in_features
        self.fc1 = nn.Dense(in_features, hidden_features)
        self.act = act_layer()
        self.fc2 = nn.Dense(hidden_features, out_features)
        self.drop = nn.Dropout(1.0-drop)

    def construct(self, x):
        x = self.fc1(x)
        x = self.act(x)
        x = self.drop(x)
        x = self.fc2(x)
        x = self.drop(x)
        return x

class BigModel(nn.Cell):
    def __init__(self, model1, model2, in_feature):
        super().__init__()
        self.model1 = model1
        self.model2 = model2
        self.cat_calssifier = Mlp(in_features=in_feature, hidden_features=1000, out_features=1000,act_layer=nn.GELU,drop=0.)
        self.concat = P.Concat(axis=1)
    
    def construct(self, x):
        f1, f2, p1, p2 = self.model1(x)
        f3, p3 = self.model2(x)
        p4 = self.cat_calssifier(self.concat([f1, f2, f3]))
        # p4 = self.cat_calssifier(torch.cat([f1, f2, f3], dim=1))
        return [p1, p2, p3, p4]

    

# class Attention(nn.Cell):
#     def __init__(self, dim, num_heads=8, qkv_bias=False, qk_scale=None, attn_drop=0., proj_drop=0.):
#         super().__init__()
#         self.num_heads = num_heads
#         head_dim = dim // num_heads
#         # NOTE scale factor was wrong in my original version, can set manually to be compat with prev weights
#         self.scale = qk_scale or head_dim ** -0.5

#         self.qkv = nn.Dense(dim, dim * 3, has_bias=qkv_bias)
#         self.attn_drop = nn.Dropout(1.0-attn_drop)
#         self.proj = nn.Dense(dim, dim)
#         self.proj_drop = nn.Dropout(1.0-proj_drop)

#     def construct(self, x):
#         B, N, C = x.shape
#         qkv = self.qkv(x).reshape(B, N, 3, self.num_heads, C // self.num_heads).permute(2, 0, 3, 1, 4)
#         q, k, v = qkv[0], qkv[1], qkv[2]  # make torchscript happy (cannot use tensor as tuple)

#         attn = (q @ k.transpose(-2, -1)) * self.scale
#         attn = attn.softmax(dim=-1)
#         attn = self.attn_drop(attn)

#         x = (attn @ v).transpose((0, 2, 1)).reshape(B, N, C)
#         x = self.proj(x)
#         x = self.proj_drop(x)
#         return x

class Attention(nn.Cell):
    """Multi-head Attention"""

    def __init__(self, dim, hidden_dim=None, num_heads=8, qkv_bias=False, qk_scale=None, attn_drop=0., proj_drop=0.):
        super(Attention, self).__init__()
        hidden_dim = hidden_dim or dim
        self.hidden_dim = hidden_dim
        self.num_heads = num_heads
        head_dim = hidden_dim // num_heads
        self.head_dim = head_dim
        self.scale = head_dim ** -0.5

        self.qk = nn.Dense(dim, hidden_dim * 2, has_bias=qkv_bias)
        self.v = nn.Dense(dim, hidden_dim, has_bias=qkv_bias)
        self.softmax = nn.Softmax(axis=-1)
        self.batmatmul_trans_b = P.BatchMatMul(transpose_b=True)
        self.attn_drop = nn.Dropout(1. - attn_drop)
        self.batmatmul = P.BatchMatMul()
        self.proj = nn.Dense(hidden_dim, dim)
        self.proj_drop = nn.Dropout(1. - proj_drop)

        self.transpose = P.Transpose()
        self.reshape = P.Reshape()

    def construct(self, x):
        """Multi-head Attention"""
        B, N, _ = x.shape
        qk = self.transpose(self.reshape(self.qk(x), (B, N, 2, self.num_heads, self.head_dim)), (2, 0, 3, 1, 4))
        q, k = qk[0], qk[1]
        v = self.transpose(self.reshape(self.v(x), (B, N, self.num_heads, self.head_dim)), (0, 2, 1, 3))

        attn = self.softmax(self.batmatmul_trans_b(q, k) * self.scale)
        attn = self.attn_drop(attn)
        x = self.reshape(self.transpose(self.batmatmul(attn, v), (0, 2, 1, 3)), (B, N, -1))
        x = self.proj(x)
        x = self.proj_drop(x)
        return x


class Block(nn.Cell):

    def __init__(self, dim, num_heads, mlp_ratio=4., qkv_bias=False, qk_scale=None, drop=0., attn_drop=0.,
                 drop_path=0., act_layer=nn.GELU, norm_layer=partial(nn.LayerNorm, epsilon=1e-6)):
        super().__init__()
        self.norm1 = norm_layer([dim])
        self.attn = Attention(
            dim, num_heads=num_heads, qkv_bias=qkv_bias, qk_scale=qk_scale, attn_drop=attn_drop, proj_drop=drop)
        # NOTE: drop path for stochastic depth, we shall see if this is better than dropout here
        self.drop_path = DropPath(drop_path) if drop_path > 0. else P.Identity() # Is the usage of Identity correct? 
        self.norm2 = norm_layer([dim])
        mlp_hidden_dim = int(dim * mlp_ratio)
        self.mlp = Mlp(in_features=dim, hidden_features=mlp_hidden_dim, act_layer=act_layer, drop=drop)

    def construct(self, x):
        x = x + self.drop_path(self.attn(self.norm1(x)))
        x = x + self.drop_path(self.mlp(self.norm2(x)))
        return x


class ConvBlock(nn.Cell):

    def __init__(self, inplanes, outplanes, stride=1, res_conv=False, act_layer=nn.ReLU, groups=1,
                 norm_layer=partial(nn.BatchNorm2d, eps=1e-6), drop_block=None, drop_path=0.):
        super(ConvBlock, self).__init__()

        expansion = 4
        med_planes = outplanes // expansion

        self.conv1 = nn.Conv2d(inplanes, med_planes, kernel_size=1, stride=1, padding=0, has_bias=False, pad_mode="pad")
        self.bn1 = norm_layer(med_planes)
        # self.act1 = act_layer(inplace=True)
        self.act1 = act_layer()

        self.conv2 = nn.Conv2d(med_planes, med_planes, kernel_size=3, stride=stride, group=groups, padding=1, has_bias=False, pad_mode="pad")
        self.bn2 = norm_layer(med_planes)
        # self.act2 = act_layer(inplace=True)
        self.act2 = act_layer()

        self.conv3 = nn.Conv2d(med_planes, outplanes, kernel_size=1, stride=1, padding=0, has_bias=False, pad_mode="pad")
        self.bn3 = norm_layer(outplanes)
        # self.act3 = act_layer(inplace=True)
        self.act3 = act_layer()

        if res_conv:
            self.residual_conv = nn.Conv2d(inplanes, outplanes, kernel_size=1, stride=stride, padding=0, has_bias=False, pad_mode="pad")
            self.residual_bn = norm_layer(outplanes)

        self.res_conv = res_conv
        self.drop_block = drop_block
        self.drop_path = DropPath(drop_path) if drop_path > 0. else P.Identity()

        self.zeros_ = mindspore.common.initializer.Zero()

    def zero_init_last_bn(self):
        self.zeros_(self.bn3.weight) # fill the input tensor with the scalar value 0

    def construct(self, x, x_t=None, return_x_2=True):
        residual = x

        x = self.conv1(x)
        x = self.bn1(x)
        if self.drop_block is not None:
            x = self.drop_block(x)
        x = self.act1(x)

        x = self.conv2(x) if x_t is None else self.conv2((x + x_t)*0.5)
        x = self.bn2(x)
        if self.drop_block is not None:
            x = self.drop_block(x)
        x2 = self.act2(x)

        x = self.conv3(x2)
        x = self.bn3(x)
        if self.drop_block is not None:
            x = self.drop_block(x)

        if self.drop_path is not None:
            x = self.drop_path(x)

        if self.res_conv:
            residual = self.residual_conv(residual)
            residual = self.residual_bn(residual)

        x += residual
        x = self.act3(x)

        if return_x_2:
            return x, x2
        else:
            return x


class FCUDown(nn.Cell):
    """ CNN feature maps -> Transformer patch embeddings
    """

    def __init__(self, inplanes, outplanes, dw_stride, act_layer=nn.GELU,
                 norm_layer=partial(nn.LayerNorm, epsilon=1e-6), cls_token=True):
        super(FCUDown, self).__init__()
        self.dw_stride = dw_stride
        self.cls_token = cls_token

        self.conv_project = nn.Conv2d(inplanes, outplanes, kernel_size=1, stride=1, padding=0, has_bias=True, pad_mode="pad") # the default parameter should be checked
        self.sample_pooling = nn.AvgPool2d(kernel_size=dw_stride, stride=dw_stride) # the default parameter should be checked

        self.ln = norm_layer([outplanes])
        self.act = act_layer()
        # print(inplanes, outplanes)

        self.concat = P.Concat(axis=1)

    def construct(self, x, x_t):
        x = self.conv_project(x)  # [N, C, H, W]
        # print(x.shape, self.dw_stride)
        tmp = self.sample_pooling(x)
        tmp1 = msimm.flatten(tmp, 2)
        x = tmp1.transpose((0, 2, 1))
        x = self.ln(x)
        x = self.act(x)
        if self.cls_token:
            # x = torch.cat([x_t[:, 0][:, None, :], x], dim=1)
            x = self.concat([x_t[:, 0][:, None, :], x])
        return x


class FCUUp(nn.Cell):
    """ Transformer patch embeddings -> CNN feature maps
    """

    def __init__(self, inplanes, outplanes, up_stride, act_layer=nn.ReLU,
                 norm_layer=partial(nn.BatchNorm2d, eps=1e-6), cls_token=True):
        super(FCUUp, self).__init__()

        self.up_stride = up_stride
        self.conv_project = nn.Conv2d(inplanes, outplanes, kernel_size=1, stride=1, padding=0, has_bias=True, pad_mode="pad")
        self.bn = norm_layer(outplanes)
        self.act = act_layer()
        self.cls_token = cls_token
        self.resize_bilinear = nn.ResizeBilinear()

    def construct(self, x, H, W):
        B, _, C = x.shape
        # [N, 197, 384] -> [N, 196, 384] -> [N, 384, 196] -> [N, 384, 14, 14]
        if self.cls_token:
            x_r = x[:, 1:].transpose((0, 2, 1)).reshape(B, C, H, W)
        else:
            x_r = x.transpose((0, 2, 1)).reshape(B, C, H, W)
        tmp = self.conv_project(x_r)
        tmp1 = self.bn(tmp)
        x_r = self.act(self.bn(self.conv_project(x_r)))

        # print("in FCUUP:", B, C, H, W, x_r.shape)

        # return F.interpolate(x_r, size=(H * self.up_stride, W * self.up_stride)) 
        return self.resize_bilinear(x_r, size=(H * self.up_stride, W * self.up_stride)) 


class Med_ConvBlock(nn.Cell):
    """ special case for Convblock with down sampling,
    """

    def __init__(self, inplanes, act_layer=nn.ReLU, groups=1, norm_layer=partial(nn.BatchNorm2d, eps=1e-6),
                 drop_block=None, drop_path=None):

        super(Med_ConvBlock, self).__init__()

        expansion = 4
        med_planes = inplanes // expansion

        self.conv1 = nn.Conv2d(inplanes, med_planes, kernel_size=1, stride=1, padding=0, has_bias=False, pad_mode="pad")
        self.bn1 = norm_layer(med_planes)
        self.act1 = act_layer()

        self.conv2 = nn.Conv2d(med_planes, med_planes, kernel_size=3, stride=1, group=groups, padding=1, has_bias=False, pad_mode="pad")
        self.bn2 = norm_layer(med_planes)
        self.act2 = act_layer()

        self.conv3 = nn.Conv2d(med_planes, inplanes, kernel_size=1, stride=1, padding=0, has_bias=False, pad_mode="pad")
        self.bn3 = norm_layer(inplanes)
        self.act3 = act_layer()

        self.drop_block = drop_block
        self.drop_path = drop_path

        self.zeros_ = mindspore.common.initializer.Zero()

    def zero_init_last_bn(self): # fill the input tensor with the scalar value 0
        self.zeros_(self.bn3.weight)

    def construct(self, x):
        residual = x

        x = self.conv1(x)
        x = self.bn1(x)
        if self.drop_block is not None:
            x = self.drop_block(x)
        x = self.act1(x)

        x = self.conv2(x)
        x = self.bn2(x)
        if self.drop_block is not None:
            x = self.drop_block(x)
        x = self.act2(x)

        x = self.conv3(x)
        x = self.bn3(x)
        if self.drop_block is not None:
            x = self.drop_block(x)

        if self.drop_path is not None:
            x = self.drop_path(x)

        x += residual
        x = self.act3(x)

        return x


class ConvTransBlock(nn.Cell):
    """
    Basic module for ConvTransformer, keep feature maps for CNN block and patch embeddings for transformer encoder block
    """

    def __init__(self, inplanes, outplanes, res_conv, stride, dw_stride, embed_dim, num_heads=12, mlp_ratio=4.,
                 qkv_bias=False, qk_scale=None, drop_rate=0., attn_drop_rate=0., drop_path_rate=0.,
                 last_fusion=False, num_med_block=0, groups=1, cls_token=True):

        super(ConvTransBlock, self).__init__()
        expansion = 4
        # print('convtransblock', inplanes, outplanes)
        self.cnn_block = ConvBlock(inplanes=inplanes, outplanes=outplanes, res_conv=res_conv, stride=stride,
                                   groups=groups, drop_path=drop_path_rate)

        if last_fusion:
            self.fusion_block = ConvBlock(inplanes=outplanes, outplanes=outplanes, stride=2, res_conv=True,
                                          groups=groups, drop_path=drop_path_rate)
        else:
            self.fusion_block = ConvBlock(inplanes=outplanes, outplanes=outplanes, groups=groups, drop_path=drop_path_rate)

        if num_med_block > 0:
            self.med_block = []
            for i in range(num_med_block):
                self.med_block.append(Med_ConvBlock(inplanes=outplanes, groups=groups))
            # self.med_block = nn.ModuleList(self.med_block) # should be checked
            self.med_block = nn.CellList(self.med_block)

        self.squeeze_block = FCUDown(inplanes=outplanes // expansion,
                                     outplanes=embed_dim, dw_stride=dw_stride, cls_token=cls_token)

        self.expand_block = FCUUp(inplanes=embed_dim,
                                  outplanes=outplanes // expansion, up_stride=dw_stride, cls_token=cls_token)

        self.trans_block = Block(
            dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
            drop=drop_rate, attn_drop=attn_drop_rate, drop_path=drop_path_rate)

        self.dw_stride = dw_stride
        self.embed_dim = embed_dim
        self.num_med_block = num_med_block
        self.last_fusion = last_fusion

    def construct(self, x, x_t):
        # print(x.shape)
        x, x2 = self.cnn_block(x)
        # print(x.shape, x2.shape)

        _, _, H, W = x2.shape

        x_st = self.squeeze_block(x2, x_t)
        # print('x_t:', x_t.shape)
        # print('x_st:', x_st.shape)

        x_t = self.trans_block(x_st + x_t)
        # print('x_t:', x_t.shape)

        if self.num_med_block > 0:
            for m in self.med_block:
                x = m(x)

        x_t_r = self.expand_block(x_t, H // self.dw_stride, W // self.dw_stride)
        # print("shapes:", x.shape, x_t_r.shape, x_t.shape, H, W)
        x = self.fusion_block(x, x_t_r, return_x_2=False)
        # print(x.shape, x_t.shape)
        return x, x_t


class Conformer(nn.Cell):

    def __init__(self, patch_size=16, in_chans=3, num_classes=1000, base_channel=64, channel_ratio=4, num_med_block=0,
                 embed_dim=768, depth=12, num_heads=12, mlp_ratio=4., qkv_bias=False, qk_scale=None,
                 drop_rate=0., attn_drop_rate=0., drop_path_rate=0., cls_token=True):

        # Transformer
        super().__init__()
        self.num_classes = num_classes
        self.num_features = self.embed_dim = embed_dim  # num_features for consistency with other models
        assert depth % 3 == 0
        self.zeros = P.Zeros()
        self.linspace = P.LinSpace()

        self.cls_token_flag = cls_token
        if self.cls_token_flag:
            self.cls_token = mindspore.Parameter(self.zeros((1, 1, embed_dim), mindspore.float32))
        
        self.trans_dpr = [x for x in self.linspace(Tensor(0, mindspore.float32), Tensor(drop_path_rate, mindspore.float32), depth)]  # stochastic depth decay rule

        # Classifier head
        self.trans_norm = nn.LayerNorm([embed_dim], epsilon=1e-05)
        self.trans_cls_head = nn.Dense(embed_dim, num_classes) if num_classes > 0 else P.Identity()
        # self.pooling = P.AdaptiveAvgPool2D(1)
        self.pooling = nn.AvgPool2d(kernel_size=7, stride=7)
        self.conv_cls_head = nn.Dense(int(256 * channel_ratio), num_classes)

        # Stem stage: get the feature maps by conv block (copied form resnet.py)
        self.conv1 = nn.Conv2d(in_chans, 64, kernel_size=7, stride=2, padding=3, has_bias=False, pad_mode="pad")  # 1 / 2 [112, 112]
        self.bn1 = nn.BatchNorm2d(64)
        self.act1 = nn.ReLU()
        # self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)  # 1 / 4 [56, 56]
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, pad_mode="same")  # paramter padding should be checked

        # concat of mindspore
        self.concat = P.Concat(axis=1)

        # 1 stage
        stage_1_channel = int(base_channel * channel_ratio)
        trans_dw_stride = patch_size // 4
        self.conv_1 = ConvBlock(inplanes=64, outplanes=stage_1_channel, res_conv=True, stride=1)
        self.trans_patch_conv = nn.Conv2d(64, embed_dim, kernel_size=trans_dw_stride, stride=trans_dw_stride, padding=0, has_bias=True, pad_mode="pad")
        self.trans_1 = Block(dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias,
                             qk_scale=qk_scale, drop=drop_rate, attn_drop=attn_drop_rate, drop_path=self.trans_dpr[0],
                             )

        # 2~4 stage
        init_stage = 2
        fin_stage = depth // 3 + 1
        for i in range(init_stage, fin_stage):
            self.insert_child_to_cell('conv_trans_' + str(i),
                            ConvTransBlock(
                                stage_1_channel, stage_1_channel, False, 1, dw_stride=trans_dw_stride,
                                embed_dim=embed_dim,
                                num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                drop_rate=drop_rate, attn_drop_rate=attn_drop_rate,
                                drop_path_rate=self.trans_dpr[i - 1],
                                num_med_block=num_med_block,
                                cls_token=self.cls_token_flag
                            )
                            )

        stage_2_channel = int(base_channel * channel_ratio * 2)
        # 5~8 stage
        init_stage = fin_stage  # 5
        fin_stage = fin_stage + depth // 3  # 9
        for i in range(init_stage, fin_stage):
            s = 2 if i == init_stage else 1
            in_channel = stage_1_channel if i == init_stage else stage_2_channel
            res_conv = True if i == init_stage else False
            self.insert_child_to_cell('conv_trans_' + str(i),
                            ConvTransBlock(
                                in_channel, stage_2_channel, res_conv, s, dw_stride=trans_dw_stride // 2,
                                embed_dim=embed_dim,
                                num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                drop_rate=drop_rate, attn_drop_rate=attn_drop_rate,
                                drop_path_rate=self.trans_dpr[i - 1],
                                num_med_block=num_med_block,
                                cls_token=self.cls_token_flag
                            )
                            )

        stage_3_channel = int(base_channel * channel_ratio * 2 * 2)
        # 9~12 stage
        init_stage = fin_stage  # 9
        fin_stage = fin_stage + depth // 3  # 13
        for i in range(init_stage, fin_stage):
            s = 2 if i == init_stage else 1
            in_channel = stage_2_channel if i == init_stage else stage_3_channel
            res_conv = True if i == init_stage else False
            last_fusion = True if i == depth else False
            self.insert_child_to_cell('conv_trans_' + str(i),
                            ConvTransBlock(
                                in_channel, stage_3_channel, res_conv, s, dw_stride=trans_dw_stride // 4,
                                embed_dim=embed_dim,
                                num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                drop_rate=drop_rate, attn_drop_rate=attn_drop_rate,
                                drop_path_rate=self.trans_dpr[i - 1],
                                num_med_block=num_med_block, last_fusion=last_fusion,
                                cls_token=self.cls_token_flag
                            )
                            )
        self.fin_stage = fin_stage
        self.child_name = list()
        for i in range(2, self.fin_stage):
            self.child_name.append("conv_trans_"+str(i))

        if self.cls_token_flag:
            trunc_normal_(self.cls_token, std=.02)

        # self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, nn.Dense):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, nn.Dense) and m.bias is not None:
                constant_ = Init.Constant(0.0) 
                constant_(m.bias)
        elif isinstance(m, nn.LayerNorm):
            constant_bias = Init.Constant(0.0)
            constant_weight = Init.Constant(1.0)
            constant_bias(m.bias)
            constant_weight(m.weight)
        elif isinstance(m, nn.Conv2d):
            henormal_weight = Init.HeNormal(mode='fan_out', nonlinearity='relu')
            henormal_weight(m.weight)
            # nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
        elif isinstance(m, nn.BatchNorm2d):
            constant_bias = Init.Constant(0.0)
            constant_weight = Init.Constant(1.0)
            constant_bias(m.bias)
            constant_weight(m.weight)
        elif isinstance(m, nn.GroupNorm):
            constant_bias = Init.Constant(0.0)
            constant_weight = Init.Constant(1.0)
            constant_bias(m.bias)
            constant_weight(m.weight)

    # @torch.jit.ignore # should be checked
    # def no_weight_decay(self):
    #     return {'cls_token', 'pos_embed'}

    def construct(self, x):
        B = x.shape[0]
        cls_tokens = None
        if self.cls_token_flag:
            # cls_tokens = self.cls_token.expand(B, -1, -1) #should be checked
            broadcastto = P.BroadcastTo((B, -1, -1))
            cls_tokens = broadcastto(self.cls_token)

        # pdb.set_trace()
        # stem stage [N, 3, 224, 224] -> [N, 64, 56, 56]
        x_base = self.maxpool(self.act1(self.bn1(self.conv1(x))))

        # 1 stage
        x = self.conv_1(x_base, return_x_2=False)

        tmp = self.trans_patch_conv(x_base)
        tmp1 = msimm.flatten(tmp, 2)

        x_t = tmp1.transpose((0, 2, 1))
        if self.cls_token_flag:
            x_t = self.concat([cls_tokens, x_t])
            # x_t = torch.cat([cls_tokens, x_t], dim=1)
        x_t = self.trans_1(x_t)

        # 2 ~ final
        for name in self.child_name:
            # x, x_t = eval('self.conv_trans_' + str(i))(x, x_t) # eval is not supported in graph mode
            # name = "conv_trans_" + str(i)
            x, x_t = self.name_cells()[name](x, x_t)
            # x, x_t = self.name_cells_name(name)(x, x_t)

        # conv classification
        tmp = self.pooling(x)
        x_p = msimm.flatten(tmp, 1)
        conv_cls = self.conv_cls_head(x_p)

        # trans classification
        x_t = self.trans_norm(x_t)
        if self.cls_token_flag:
            tran_cls = self.trans_cls_head(x_t[:, 0])
        else:
            tran_cls = self.trans_cls_head(x_t.mean(axis=1))
        return x_p, x_t[:, 0], conv_cls, tran_cls

class Conformer_res101(nn.Cell):

    def __init__(self, patch_size=16, in_chans=3, num_classes=1000, base_channel=64, channel_ratio=4, num_med_block=0,
                 embed_dim=768, depth=12, num_heads=12, mlp_ratio=4., qkv_bias=False, qk_scale=None,
                 drop_rate=0., attn_drop_rate=0., drop_path_rate=0., cls_token=False):

        # Transformer
        super().__init__()
        self.num_classes = num_classes
        self.num_features = self.embed_dim = embed_dim  # num_features for consistency with other models
        # assert depth % 3 == 0

        self.zeros = P.Zeros()
        self.linspace = P.LinSpace()

        self.cls_token_flag = cls_token
        if self.cls_token_flag:
            self.cls_token = mindspore.Parameter(self.zeros((1, 1, embed_dim), mindspore.float32))
        self.trans_dpr = [x for x in self.linspace(Tensor(0, mindspore.float32), Tensor(drop_path_rate, mindspore.float32), depth)]  # stochastic depth decay rule

        # Classifier head
        self.trans_norm = nn.LayerNorm([embed_dim], epsilon=1e-05)
        self.trans_cls_head = nn.Dense(embed_dim, num_classes) if num_classes > 0 else P.Identity()
        # self.pooling = P.AdaptiveAvgPool2D(1)
        self.pooling = nn.AvgPool2d(kernel_size=7, stride=7)
        self.conv_cls_head = nn.Dense(int(base_channel*8*channel_ratio), num_classes)

        # Stem stage: get the feature maps by conv block (copied form resnet.py)
        self.conv1 = nn.Conv2d(in_chans, 64, kernel_size=7, stride=2, padding=3, has_bias=False, pad_mode="pad")  # 1 / 2 [112, 112]
        self.bn1 = nn.BatchNorm2d(64)
        self.act1 = nn.ReLU()
        # self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)  # 1 / 4 [56, 56]
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, pad_mode="same")

        # concat of mindspore
        self.concat = P.Concat(axis=1)

        # 5 stages in total: 1 3 4 23 3
        # 1 stage
        stage_1_channel = int(base_channel * channel_ratio)
        trans_dw_stride = patch_size // 4
        self.conv_1 = ConvBlock(inplanes=64, outplanes=stage_1_channel, res_conv=True, stride=1)
        self.trans_patch_conv = nn.Conv2d(64, embed_dim, kernel_size=trans_dw_stride, stride=trans_dw_stride, padding=0, has_bias=True, pad_mode="pad")
        self.trans_1 = Block(dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias,
                             qk_scale=qk_scale, drop=drop_rate, attn_drop=attn_drop_rate, drop_path=self.trans_dpr[0],
                             )

        # 2~4 stage
        init_stage = 2
        fin_stage = 4+1
        for i in range(init_stage, fin_stage):
            self.insert_child_to_cell('conv_trans_' + str(i),
                            ConvTransBlock(
                                stage_1_channel, stage_1_channel, False, 1, dw_stride=trans_dw_stride,
                                embed_dim=embed_dim,
                                num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                drop_rate=drop_rate, attn_drop_rate=attn_drop_rate,
                                drop_path_rate=self.trans_dpr[i - 1],
                                num_med_block=num_med_block,
                                cls_token=self.cls_token_flag
                            )
                            )

        stage_2_channel = int(base_channel * channel_ratio * 2)
        # 5~8 stage
        init_stage = fin_stage  # 5
        fin_stage = 8+1
        for i in range(init_stage, fin_stage):
            s = 2 if i == init_stage else 1
            in_channel = stage_1_channel if i == init_stage else stage_2_channel
            res_conv = True if i == init_stage else False
            self.insert_child_to_cell('conv_trans_' + str(i),
                            ConvTransBlock(
                                in_channel, stage_2_channel, res_conv, s, dw_stride=trans_dw_stride // 2,
                                embed_dim=embed_dim,
                                num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                drop_rate=drop_rate, attn_drop_rate=attn_drop_rate,
                                drop_path_rate=self.trans_dpr[i - 1],
                                num_med_block=num_med_block,
                                cls_token=self.cls_token_flag
                            )
                            )

        stage_3_channel = int(base_channel * channel_ratio * 2 * 2)
        # 9~31 stage
        stage_4_channel = int(base_channel * channel_ratio * 2 * 2 * 2)
        init_stage = fin_stage  # 5
        fin_stage = 30+2
        for i in range(init_stage, fin_stage):
            if i == 10:
                debug = 1
            s = 2 if i == init_stage else 1
            in_channel = stage_2_channel if i == init_stage else stage_3_channel
            out_channel = stage_4_channel if i == depth else stage_3_channel
            res_conv = True if (i == init_stage or i == depth) else False
            last_fusion = True if i == depth else False
            self.insert_child_to_cell('conv_trans_' + str(i),
                            ConvTransBlock(
                                in_channel, out_channel, res_conv, s, dw_stride=trans_dw_stride // 4,
                                embed_dim=embed_dim,
                                num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                drop_rate=drop_rate, attn_drop_rate=attn_drop_rate,
                                drop_path_rate=self.trans_dpr[i - 1],
                                num_med_block=num_med_block, last_fusion=last_fusion,
                                cls_token=self.cls_token_flag
                            )
                            )

        self.fin_stage = fin_stage

        if self.cls_token_flag:
            trunc_normal_(self.cls_token, std=.02)

        # self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, nn.Dense):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, nn.Dense) and m.bias is not None:
                constant_ = Init.Constant(0.0)
                constant_(m.bias)
        elif isinstance(m, nn.LayerNorm):
            constant_bias = Init.Constant(0.0)
            constant_weight = Init.Constant(1.0)
            constant_bias(m.bias)
            constant_weight(m.weight)
        elif isinstance(m, nn.Conv2d):
            henormal_weight = Init.HeNormal(mode='fan_out', nonlinearity='relu')
            henormal_weight(m.weight)
        elif isinstance(m, nn.BatchNorm2d):
            constant_bias = Init.Constant(0.0)
            constant_weight = Init.Constant(1.0)
            constant_bias(m.bias)
            constant_weight(m.weight)
        elif isinstance(m, nn.GroupNorm):
            constant_bias = Init.Constant(0.0)
            constant_weight = Init.Constant(1.0)
            constant_bias(m.bias)
            constant_weight(m.weight)

    # @torch.jit.ignore
    # def no_weight_decay(self):
    #     return {'cls_token', 'pos_embed'}

    def construct(self, x):
        B = x.shape[0]
        if self.cls_token_flag:
            # cls_tokens = self.cls_token.expand(B, -1, -1)
            broadcastto = P.BroadcastTo((B, -1, -1))
            cls_tokens = broadcastto(self.cls_token)

        # pdb.set_trace()
        # stem stage [N, 3, 224, 224] -> [N, 64, 56, 56]
        x_base = self.maxpool(self.act1(self.bn1(self.conv1(x))))

        # 1 stage
        # [N, 64, 56, 56] -> [N, 384, 56, 56]
        x = self.conv_1(x_base, return_x_2=False)

        # [N, 64, 56, 56] -> [N, 196, 576]
        tmp = self.trans_patch_conv(x_base)
        tmp1 = msimm.flatten(tmp, 2)
        x_t = tmp1.transpose((0, 2, 1))
        if self.cls_token_flag:
            x_t = self.concat([cls_tokens, x_t])
            # x_t = torch.cat([cls_tokens, x_t], dim=1)
        # [N, 196, 576]
        x_t = self.trans_1(x_t)

        # 2 ~ final
        for i in range(2, self.fin_stage):
            # print(i, '-------')
            # x, x_t = eval('self.conv_trans_' + str(i))(x, x_t)
            name = 'conv_trans_' + str(i)
            x, x_t = self.name_cells()[name](x, x_t)

        # conv classification
        tmp = self.pooling(x)
        x_p = msimm.flatten(tmp, 1)
        conv_cls = self.conv_cls_head(x_p)

        # trans classification
        x_t = self.trans_norm(x_t)
        if self.cls_token_flag:
            tran_cls = self.trans_cls_head(x_t[:, 0])
        else:
            tran_cls = self.trans_cls_head(x_t.mean(axis=1))
        return x_p, x_t.mean(axis=1), conv_cls, tran_cls

class Conformer_res152(nn.Cell):

    def __init__(self, patch_size=16, in_chans=3, num_classes=1000, base_channel=64, channel_ratio=4, num_med_block=0,
                 embed_dim=768, depth=12, num_heads=12, mlp_ratio=4., qkv_bias=False, qk_scale=None,
                 drop_rate=0., attn_drop_rate=0., drop_path_rate=0., cls_token=False):

        # Transformer
        super().__init__()
        self.num_classes = num_classes
        self.num_features = self.embed_dim = embed_dim  # num_features for consistency with other models
        # assert depth % 3 == 0
        self.zeros = P.Zeros()
        self.linspace = P.LinSpace()

        self.cls_token_flag = cls_token
        if self.cls_token_flag:
            self.cls_token = mindspore.Parameter(self.zeros((1, 1, embed_dim), mindspore.float32))
        self.trans_dpr = [x for x in self.linspace(Tensor(0, mindspore.float32), Tensor(drop_path_rate, mindspore.float32), depth)]  # stochastic depth decay rule

        # Classifier head
        self.trans_norm = nn.LayerNorm([embed_dim], epsilon=1e-05)
        self.trans_cls_head = nn.Dense(embed_dim, num_classes) if num_classes > 0 else P.Identity()
        # self.pooling = P.AdaptiveAvgPool2D(1)
        self.pooling = nn.AvgPool2d(kernel_size=7, stride=7)
        self.conv_cls_head = nn.Dense(int(base_channel*8*channel_ratio), num_classes)

        # Stem stage: get the feature maps by conv block (copied form resnet.py)
        self.conv1 = nn.Conv2d(in_chans, 64, kernel_size=7, stride=2, padding=3, has_bias=False, pad_mode="pad")  # 1 / 2 [112, 112]
        self.bn1 = nn.BatchNorm2d(64)
        self.act1 = nn.ReLU()
        # self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)  # 1 / 4 [56, 56]
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, pad_mode="same") 

        # concat of mindspore
        self.concat = P.Concat(axis=1)

        # 5 stages in total: 1 3 8 36 1
        # 1 stage
        stage_1_channel = int(base_channel * channel_ratio)
        trans_dw_stride = patch_size // 4
        self.conv_1 = ConvBlock(inplanes=64, outplanes=stage_1_channel, res_conv=True, stride=1)
        self.trans_patch_conv = nn.Conv2d(64, embed_dim, kernel_size=trans_dw_stride, stride=trans_dw_stride, padding=0, has_bias=True, pad_mode="pad")
        self.trans_1 = Block(dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias,
                             qk_scale=qk_scale, drop=drop_rate, attn_drop=attn_drop_rate, drop_path=self.trans_dpr[0],
                             )

        # 2~4 stage
        init_stage = 2
        fin_stage = 4+1
        for i in range(init_stage, fin_stage):
            self.insert_child_to_cell('conv_trans_' + str(i),
                            ConvTransBlock(
                                stage_1_channel, stage_1_channel, False, 1, dw_stride=trans_dw_stride,
                                embed_dim=embed_dim,
                                num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                drop_rate=drop_rate, attn_drop_rate=attn_drop_rate,
                                drop_path_rate=self.trans_dpr[i - 1],
                                num_med_block=num_med_block,
                                cls_token=self.cls_token_flag
                            )
                            )

        stage_2_channel = int(base_channel * channel_ratio * 2)
        # 5~12 stage
        init_stage = fin_stage  # 5
        fin_stage = 4+12+1
        for i in range(init_stage, fin_stage):
            s = 2 if i == init_stage else 1
            in_channel = stage_1_channel if i == init_stage else stage_2_channel
            res_conv = True if i == init_stage else False
            self.insert_child_to_cell('conv_trans_' + str(i),
                            ConvTransBlock(
                                in_channel, stage_2_channel, res_conv, s, dw_stride=trans_dw_stride // 2,
                                embed_dim=embed_dim,
                                num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                drop_rate=drop_rate, attn_drop_rate=attn_drop_rate,
                                drop_path_rate=self.trans_dpr[i - 1],
                                num_med_block=num_med_block,
                                cls_token=self.cls_token_flag
                            )
                            )

        stage_3_channel = int(base_channel * channel_ratio * 2 * 2)
        # 13~48 stage
        stage_4_channel = int(base_channel * channel_ratio * 2 * 2 * 2)
        init_stage = fin_stage  # 5
        fin_stage = 23+48+1
        for i in range(init_stage, fin_stage):
            # if i == 10:
            #     debug = 1
            s = 2 if i == init_stage else 1
            in_channel = stage_2_channel if i == init_stage else stage_3_channel
            out_channel = stage_4_channel if i == depth else stage_3_channel
            res_conv = True if (i == init_stage or i == depth) else False
            last_fusion = True if i == depth else False
            self.insert_child_to_cell('conv_trans_' + str(i),
                            ConvTransBlock(
                                in_channel, out_channel, res_conv, s, dw_stride=trans_dw_stride // 4,
                                embed_dim=embed_dim,
                                num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                drop_rate=drop_rate, attn_drop_rate=attn_drop_rate,
                                drop_path_rate=self.trans_dpr[i - 1],
                                num_med_block=num_med_block, last_fusion=last_fusion,
                                cls_token=self.cls_token_flag
                            )
                            )

        self.fin_stage = fin_stage

        if self.cls_token_flag:
            trunc_normal_(self.cls_token, std=.02)

        # self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, nn.Dense):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, nn.Dense) and m.bias is not None:
                constant_ = Init.Constant(0.0) 
                constant_(m.bias)
        elif isinstance(m, nn.LayerNorm):
            constant_bias = Init.Constant(0.0)
            constant_weight = Init.Constant(1.0)
            constant_bias(m.bias)
            constant_weight(m.weight)
        elif isinstance(m, nn.Conv2d):
            henormal_weight = Init.HeNormal(mode='fan_out', nonlinearity='relu')
            henormal_weight(m.weight)
        elif isinstance(m, nn.BatchNorm2d):
            constant_bias = Init.Constant(0.0)
            constant_weight = Init.Constant(1.0)
            constant_bias(m.bias)
            constant_weight(m.weight)
        elif isinstance(m, nn.GroupNorm):
            constant_bias = Init.Constant(0.0)
            constant_weight = Init.Constant(1.0)
            constant_bias(m.bias)
            constant_weight(m.weight)

    # @torch.jit.ignore
    # def no_weight_decay(self):
    #     return {'cls_token', 'pos_embed'}

    def construct(self, x):
        B = x.shape[0]
        if self.cls_token_flag:
            # cls_tokens = self.cls_token.expand(B, -1, -1)
            broadcastto = P.BroadcastTo((B, -1, -1))
            cls_tokens = broadcastto(self.cls_token)

        # pdb.set_trace()
        # stem stage [N, 3, 224, 224] -> [N, 64, 56, 56]
        x_base = self.maxpool(self.act1(self.bn1(self.conv1(x))))

        # 1 stage
        # [N, 64, 56, 56] -> [N, 384, 56, 56]
        x = self.conv_1(x_base, return_x_2=False)

        # [N, 64, 56, 56] -> [N, 196, 576]
        tmp = self.trans_patch_conv(x_base)
        tmp1 = msimm.flatten(tmp, 2)
        x_t = tmp1.transpose((0, 2, 1))
        if self.cls_token_flag:
            x_t = self.concat([cls_tokens, x_t])
        # [N, 196, 576]
        x_t = self.trans_1(x_t)

        # 2 ~ final
        for i in range(2, self.fin_stage):
            # print(i, '-------')
            # x, x_t = eval('self.conv_trans_' + str(i))(x, x_t)
            name = 'conv_trans_' + str(i)
            x, x_t = self.name_cells()[name](x, x_t)

        # conv classification
        tmp = self.pooling(x)
        x_p = msimm.flatten(tmp, 1)
        conv_cls = self.conv_cls_head(x_p)

        # trans classification
        x_t = self.trans_norm(x_t)
        if self.cls_token_flag:
            tran_cls = self.trans_cls_head(x_t[:, 0])
        else:
            tran_cls = self.trans_cls_head(x_t.mean(axis=1))
        return x_p, x_t.mean(axis=1), conv_cls, tran_cls

class Conformer_res152_fat(nn.Cell):
    def __init__(self, patch_size=16, in_chans=3, num_classes=1000, base_channel=64, channel_ratio=4, num_med_block=0,
                 embed_dim=768, depth=12, num_heads=12, mlp_ratio=4., qkv_bias=False, qk_scale=None,
                 drop_rate=0., attn_drop_rate=0., drop_path_rate=0., cls_token=False):

        # Transformer
        super().__init__()
        self.num_classes = num_classes
        self.num_features = self.embed_dim = embed_dim  # num_features for consistency with other models
        # assert depth % 3 == 0
        self.zeros = P.Zeros()
        self.linspace = P.LinSpace()

        self.cls_token_flag = cls_token
        if self.cls_token_flag:
            self.cls_token = mindspore.Parameter(self.zeros((1, 1, embed_dim), mindspore.float32))
        self.trans_dpr = [x for x in self.linspace(Tensor(0, mindspore.float32), Tensor(drop_path_rate, mindspore.float32), depth)]  # stochastic depth decay rule

        # Classifier head
        self.trans_norm = nn.LayerNorm([embed_dim], epsilon=1e-05)
        self.trans_cls_head = nn.Dense(embed_dim, num_classes) if num_classes > 0 else P.Identity()
        # self.pooling = P.AdaptiveAvgPool2D(1)
        self.pooling = nn.AvgPool2d(kernel_size=7, stride=7)
        self.conv_cls_head = nn.Dense(int(base_channel*8*channel_ratio), num_classes)

        # Stem stage: get the feature maps by conv block (copied form resnet.py)
        self.conv1 = nn.Conv2d(in_chans, 64, kernel_size=7, stride=2, padding=3, has_bias=False, pad_mode="pad")  # 1 / 2 [112, 112]
        self.bn1 = nn.BatchNorm2d(64)
        self.act1 = nn.ReLU()
        # self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)  # 1 / 4 [56, 56]
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, pad_mode="same")

        # concat of mindspore
        self.concat = P.Concat(axis=1)

        # 5 stages in total: 1 3 4 23 3
        # 1 stage
        stage_1_channel = int(base_channel * channel_ratio)
        trans_dw_stride = patch_size // 4
        self.conv_1 = ConvBlock(inplanes=64, outplanes=stage_1_channel, res_conv=True, stride=1)
        self.trans_patch_conv = nn.Conv2d(64, embed_dim, kernel_size=trans_dw_stride, stride=trans_dw_stride, padding=0, has_bias=True, pad_mode="pad")
        self.trans_1 = Block(dim=embed_dim, num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias,
                             qk_scale=qk_scale, drop=drop_rate, attn_drop=attn_drop_rate, drop_path=self.trans_dpr[0],
                             )

        # 2~4 stage
        init_stage = 2
        fin_stage = 4+1
        for i in range(init_stage, fin_stage):
            self.insert_child_to_cell('conv_trans_' + str(i),
                            ConvTransBlock(
                                stage_1_channel, stage_1_channel, False, 1, dw_stride=trans_dw_stride,
                                embed_dim=embed_dim,
                                num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                drop_rate=drop_rate, attn_drop_rate=attn_drop_rate,
                                drop_path_rate=self.trans_dpr[i - 1],
                                num_med_block=num_med_block,
                                cls_token=self.cls_token_flag
                            )
                            )

        stage_2_channel = int(base_channel * channel_ratio * 2)
        # 5~8 stage
        init_stage = fin_stage  # 5
        fin_stage = 8+1
        for i in range(init_stage, fin_stage):
            s = 2 if i == init_stage else 1
            in_channel = stage_1_channel if i == init_stage else stage_2_channel
            res_conv = True if i == init_stage else False
            self.insert_child_to_cell('conv_trans_' + str(i),
                            ConvTransBlock(
                                in_channel, stage_2_channel, res_conv, s, dw_stride=trans_dw_stride // 2,
                                embed_dim=embed_dim,
                                num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                drop_rate=drop_rate, attn_drop_rate=attn_drop_rate,
                                drop_path_rate=self.trans_dpr[i - 1],
                                num_med_block=num_med_block,
                                cls_token=self.cls_token_flag
                            )
                            )

        stage_3_channel = int(base_channel * channel_ratio * 2 * 2)
        # 9~31 stage
        init_stage = fin_stage  # 5
        fin_stage = 48+1
        for i in range(init_stage, fin_stage):
            s = 2 if i == init_stage else 1
            in_channel = stage_2_channel if i == init_stage else stage_3_channel
            res_conv = True if (i == init_stage) else False
            self.insert_child_to_cell('conv_trans_' + str(i),
                            ConvTransBlock(
                                in_channel, stage_3_channel, res_conv, s, dw_stride=trans_dw_stride // 4,
                                embed_dim=embed_dim,
                                num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                drop_rate=drop_rate, attn_drop_rate=attn_drop_rate,
                                drop_path_rate=self.trans_dpr[i - 1],
                                num_med_block=num_med_block,
                                cls_token=self.cls_token_flag
                            )
                            )
        stage_4_channel = int(base_channel * channel_ratio * 2 * 2 * 2)
        # 49~51 stage
        init_stage = fin_stage  # 5
        fin_stage = 51+1

        for i in range(init_stage, fin_stage):
            s = 1
            in_channel = stage_3_channel if i == init_stage else stage_4_channel
#             out_channel = stage_4_channel*2 if i == depth else stage_4_channel
#             res_conv = True if (i == init_stage or i == depth) else False
            res_conv = True if (i == init_stage) else False
            last_fusion = True if i == depth else False
            self.insert_child_to_cell('conv_trans_' + str(i),
                            ConvTransBlock(
                                in_channel, stage_4_channel, res_conv, s, dw_stride=trans_dw_stride // 4,
                                embed_dim=embed_dim,
                                num_heads=num_heads, mlp_ratio=mlp_ratio, qkv_bias=qkv_bias, qk_scale=qk_scale,
                                drop_rate=drop_rate, attn_drop_rate=attn_drop_rate,
                                drop_path_rate=self.trans_dpr[i - 1],
                                num_med_block=num_med_block, last_fusion=last_fusion,
                                cls_token=self.cls_token_flag
                            )
                            )

        self.fin_stage = fin_stage

        if self.cls_token_flag:
            trunc_normal_(self.cls_token, std=.02)

        # self.apply(self._init_weights)

    def _init_weights(self, m):
        if isinstance(m, nn.Dense):
            trunc_normal_(m.weight, std=.02)
            if isinstance(m, nn.Dense) and m.bias is not None:
                constant_ = Init.Constant(0.0) 
                constant_(m.bias)
        elif isinstance(m, nn.LayerNorm):
            constant_bias = Init.Constant(0.0)
            constant_weight = Init.Constant(1.0)
            constant_bias(m.bias)
            constant_weight(m.weight)
        elif isinstance(m, nn.Conv2d):
            henormal_weight = Init.HeNormal(mode='fan_out', nonlinearity='relu')
            henormal_weight(m.weight)
        elif isinstance(m, nn.BatchNorm2d):
            constant_bias = Init.Constant(0.0)
            constant_weight = Init.Constant(1.0)
            constant_bias(m.bias)
            constant_weight(m.weight)
        elif isinstance(m, nn.GroupNorm):
            constant_bias = Init.Constant(0.0)
            constant_weight = Init.Constant(1.0)
            constant_bias(m.bias)
            constant_weight(m.weight)

    # @torch.jit.ignore
    # def no_weight_decay(self):
    #     return {'cls_token', 'pos_embed'}

    def construct(self, x):
        B = x.shape[0]
        if self.cls_token_flag:
            # cls_tokens = self.cls_token.expand(B, -1, -1)
            broadcastto = P.BroadcastTo((B, -1, -1))
            cls_tokens = broadcastto(self.cls_token)

        # pdb.set_trace()
        # stem stage [N, 3, 224, 224] -> [N, 64, 56, 56]
        x_base = self.maxpool(self.act1(self.bn1(self.conv1(x))))

        # 1 stage
        # [N, 64, 56, 56] -> [N, 384, 56, 56]
        x = self.conv_1(x_base, return_x_2=False)

        # [N, 64, 56, 56] -> [N, 196, 576]
        tmp = self.trans_patch_conv(x_base)
        tmp1 = msimm.flatten(tmp, 2)
        x_t = tmp1.transpose((0, 2, 1))
        if self.cls_token_flag:
            x_t = self.concat([cls_tokens, x_t])
        # [N, 196, 576]
        x_t = self.trans_1(x_t)

        # 2 ~ final
        for i in range(2, self.fin_stage):
            # print(i, '-------')
            # x, x_t = eval('self.conv_trans_' + str(i))(x, x_t)
            name = 'conv_trans_' + str(i)
            x, x_t = self.name_cells()[name](x, x_t)

        # conv classification
        tmp = self.pooling(x)
        x_p = msimm.flatten(tmp, 1)
        conv_cls = self.conv_cls_head(x_p)

        # trans classification
        x_t = self.trans_norm(x_t)
        if self.cls_token_flag:
            tran_cls = self.trans_cls_head(x_t[:, 0])
        else:
            tran_cls = self.trans_cls_head(x_t.mean(axis=1))
        return x_p, x_t.mean(axis=1), conv_cls, tran_cls

