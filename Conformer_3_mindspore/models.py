# import torch
# import torch.nn as nn
import mindspore.nn as nn
from functools import partial


# from timm.models.vision_transformer import VisionTransformer, _cfg

# from vision_transformer import VisionTransformer, _cfg
from conformer import Conformer, Conformer_res101,Conformer_res152,Conformer_res152_fat
from timm.models.registry import register_model


@register_model
def Conformer_tiny_patch16(pretrained=False, **kwargs):
    model = Conformer(patch_size=16, channel_ratio=1, embed_dim=384, depth=12,
                      num_heads=6, mlp_ratio=4, qkv_bias=True, **kwargs)
    if pretrained:
        raise NotImplementedError
    return model

def simam_Conformer_tiny_patch16(pretrained=False, **kwargs):
    model = simam_Conformer(patch_size=16, channel_ratio=1, embed_dim=384, depth=12,
                      num_heads=6, mlp_ratio=4, qkv_bias=True, **kwargs)
    if pretrained:
        raise NotImplementedError
    return model

@register_model
def Conformer_small_patch16(pretrained=False, **kwargs):
    model = Conformer(patch_size=16, channel_ratio=4, embed_dim=384, depth=12,
                      num_heads=6, mlp_ratio=4, qkv_bias=True, **kwargs)
    if pretrained:
        raise NotImplementedError
    return model

@register_model
def Conformer_small_patch32(pretrained=False, **kwargs):
    model = Conformer(patch_size=32, channel_ratio=4, embed_dim=384, depth=12,
                      num_heads=6, mlp_ratio=4, qkv_bias=True, **kwargs)
    if pretrained:
        raise NotImplementedError
    return model

@register_model
def Conformer_base_patch16(pretrained=False, **kwargs):
    model = Conformer(patch_size=16, channel_ratio=6, embed_dim=576, depth=12,
                      num_heads=9, mlp_ratio=4, qkv_bias=True, cls_token=True, **kwargs)
    if pretrained:
        raise NotImplementedError
    return model

@register_model
def Conformer_base_scaling_res101_patch16(pretrained=False, **kwargs):
    # channel_ratio = 6 is based on the base channel (64)
    # embed_dim: dim after FCUdown or before FCUup.
    # depth: layer number
    # num_heads: self attention head number
    # mlp_ratio: scaling ratio of channel number after mlp

    # model = Conformer(patch_size=16, channel_ratio=6, embed_dim=576, depth=12,
    #                   num_heads=9, mlp_ratio=4, qkv_bias=True, **kwargs)

    # layer numbers are [1 3 4 23 1]
    model = Conformer_res101(patch_size=16, channel_ratio=6, embed_dim=576, depth=31,
                      num_heads=9, mlp_ratio=4, qkv_bias=True, cls_token=False, **kwargs)
    if pretrained:
        raise NotImplementedError
    return model

@register_model
def Conformer_base_scaling_res152_patch16(pretrained=False, **kwargs):
    # 603M paramters
    # channel_ratio = 6 is based on the base channel (64)
    # embed_dim: dim after FCUdown or before FCUup.
    # depth: layer number
    # num_heads: self attention head number
    # mlp_ratio: scaling ratio of channel number after mlp

    # model = Conformer(patch_size=16, channel_ratio=6, embed_dim=576, depth=12,
    #                   num_heads=9, mlp_ratio=4, qkv_bias=True, **kwargs)

    # layer numbers are [1 3 8 36 1]
    model = Conformer_res152(patch_size=16, channel_ratio=6, embed_dim=576, depth=48+23,
                      num_heads=9, mlp_ratio=4, qkv_bias=True, cls_token=False, **kwargs)
    if pretrained:
        raise NotImplementedError
    return model

@register_model
def Conformer_base_scaling_res152_fat_patch16(pretrained=False, **kwargs):
    # 510M paramters
    # channel_ratio = 6 is based on the base channel (64)
    # embed_dim: dim after FCUdown or before FCUup.
    # depth: layer number
    # num_heads: self attention head number
    # mlp_ratio: scaling ratio of channel number after mlp

    # model = Conformer(patch_size=16, channel_ratio=6, embed_dim=576, depth=12,
    #                   num_heads=9, mlp_ratio=4, qkv_bias=True, **kwargs)

    # layer numbers are [1 3 8 36 1]
    model = Conformer_res152_fat(patch_size=16, channel_ratio=6, embed_dim=576, depth=51,
                      num_heads=9, mlp_ratio=4, qkv_bias=True, cls_token=False, **kwargs)
    if pretrained:
        raise NotImplementedError
    return model
