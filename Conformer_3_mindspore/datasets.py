import os
import json

# from torchvision import datasets, transforms
# from torchvision.datasets.folder import ImageFolder, default_loader

import mindspore.dataset as ds
from mindspore.dataset import ImageFolderDataset

from timm.data.constants import IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD, IMAGENET_INCEPTION_MEAN, IMAGENET_INCEPTION_STD
from timm.data import create_transform

from mindspore.common import dtype as mstype
import mindspore.dataset.transforms.c_transforms as C
import mindspore.dataset.vision.c_transforms as vision

def classification_dataset(dataset, image_size, per_batch_size,
                           mode='train',
                           repeat_num=1,
                           drop_remainder=True,
                           transform=None,
                           target_transform=None):

    mean = [0.485 * 255, 0.456 * 255, 0.406 * 255]
    std = [0.229 * 255, 0.224 * 255, 0.225 * 255]

    if transform is None:
        if mode == 'train':
            transform_img = [
                vision.RandomCropDecodeResize(image_size, scale=(0.08, 1.0)),
                vision.RandomHorizontalFlip(prob=0.5),
                vision.Normalize(mean=mean, std=std),
                vision.HWC2CHW()
            ]
        else:
            transform_img = [
                vision.Decode(),
                vision.Resize((256, 256)),
                vision.CenterCrop(image_size),
                vision.Normalize(mean=mean, std=std),
                vision.HWC2CHW()
            ]
    else:
        transform_img = transform

    if target_transform is None:
        transform_label = [C.TypeCast(mstype.float32)]
    else:
        transform_label = target_transform


    dataset = dataset.map(operations=transform_img, input_columns="image", num_parallel_workers=8)
    dataset = dataset.map(operations=transform_label, input_columns="label", num_parallel_workers=8)

    columns_to_project = ["image", "label"]
    dataset = dataset.project(columns=columns_to_project)

    dataset = dataset.batch(per_batch_size, drop_remainder=drop_remainder)
    dataset = dataset.repeat(repeat_num)

    return dataset


class INatDataset(ImageFolderDataset):
    def __init__(self, root, train=True, year=2018, transform=None, target_transform=None,
                 category='name', loader=None): # loader = default_loader
        self.transform = transform
        self.loader = loader
        self.target_transform = target_transform
        self.year = year
        # assert category in ['kingdom','phylum','class','order','supercategory','family','genus','name']
        path_json = os.path.join(root, f'{"train" if train else "val"}{year}.json')
        with open(path_json) as json_file:
            data = json.load(json_file)

        with open(os.path.join(root, 'categories.json')) as json_file:
            data_catg = json.load(json_file)

        path_json_for_targeter = os.path.join(root, f"train{year}.json")

        with open(path_json_for_targeter) as json_file:
            data_for_targeter = json.load(json_file)

        targeter = {}
        indexer = 0
        for elem in data_for_targeter['annotations']:
            king = []
            king.append(data_catg[int(elem['category_id'])][category])
            if king[0] not in targeter.keys():
                targeter[king[0]] = indexer
                indexer += 1
        self.nb_classes = len(targeter)

        self.samples = []
        for elem in data['images']:
            cut = elem['file_name'].split('/')
            target_current = int(cut[2])
            path_current = os.path.join(root, cut[0], cut[2], cut[3])

            categors = data_catg[target_current]
            target_current_true = targeter[categors[category]]
            self.samples.append((path_current, target_current_true))

    # __getitem__ and __len__ inherited from ImageFolder


def build_dataset(is_train, args):
    # transform = build_transform(is_train, args)
    transform = None

    if args.data_set == 'CIFAR':
        # dataset = datasets.CIFAR100(args.data_path, train=is_train, transform=transform)
        dataset = ds.Cifar100Dataset(args.data_path)
        nb_classes = 100
    elif args.data_set == 'CIFAR10':
        dataset = ds.Cifar10Dataset(args.data_path)
        nb_classes = 10
    elif args.data_set == 'IMNET':
        root = os.path.join(args.data_path, 'train' if is_train else 'val')
        dataset = ds.ImageFolderDataset(root)
        nb_classes = 1000
    elif args.data_set == 'IMNET21k':
        root = os.path.join(args.data_path, 'train' if is_train else 'val')
        dataset = ds.ImageFolderDataset(root)
        nb_classes = 21843
    elif args.data_set == 'INAT':
        dataset = INatDataset(args.data_path, train=is_train, year=2018,
                              category=args.inat_category, transform=transform)
        nb_classes = dataset.nb_classes
    elif args.data_set == 'INAT19':
        dataset = INatDataset(args.data_path, train=is_train, year=2019,
                              category=args.inat_category, transform=transform)
        nb_classes = dataset.nb_classes
    elif args.data_set == 'CAR':
        root = os.path.join(args.data_path, 'train' if is_train else 'val')
        dataset = ds.ImageFolderDataset(root)
        nb_classes = 196
    return dataset, nb_classes

# def build_transform(is_train, args):
#     resize_im = args.input_size > 32
#     if is_train:
#         # this should always dispatch to transforms_imagenet_train
#         auto_augment=False if args.aa == 'None' else args.aa
#         transform = create_transform(
#             input_size=args.input_size,
#             is_training=True,
#             color_jitter=args.color_jitter,
#             auto_augment=auto_augment,
#             interpolation=args.train_interpolation,
#             re_prob=args.reprob,
#             re_mode=args.remode,
#             re_count=args.recount,
#         )
#         if not resize_im:
#             # replace RandomResizedCropAndInterpolation with
#             # RandomCrop
#             transform.transforms[0] = transforms.RandomCrop(
#                 args.input_size, padding=4)
#         return transform

#     t = []
#     if resize_im:
#         size = int((256 / 224) * args.input_size)
#         t.append(
#             transforms.Resize(size, interpolation=3),  # to maintain same ratio w.r.t. 224 images
#         )
#         t.append(transforms.CenterCrop(args.input_size))

#     t.append(transforms.ToTensor())
#     if args.norm_type == 'DEFAULT':
#         t.append(transforms.Normalize(IMAGENET_DEFAULT_MEAN, IMAGENET_DEFAULT_STD))
#     elif args.norm_type == 'INCEPTION':
#         t.append(transforms.Normalize(IMAGENET_INCEPTION_MEAN, IMAGENET_INCEPTION_STD))
#     else:
#         raise NotImplementedError
#     return transforms.Compose(t)
