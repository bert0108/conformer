#!/usr/bin/env bash
OUTPUT='./output-scale'
#CUDA_VISIBLE_DEVICES=0, python main.py  \
export CUDA_VISIBLE_DEVICES=0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
#export CUDA_VISIBLE_DEVICES=0,1

python -m torch.distributed.launch --master_port 50131 --nproc_per_node 16 --use_env train_bigmodel.py \
                --model Conformer_base_scaling_res101_patch16  \
                --in-feature 4416\
                --batch-size 32 \
                --input-size 224 \
                --data-set IMNET \
                --num_workers 16 \
                --data-path /userhome/data/imagenet \
                --output_dir ${OUTPUT}\
                --clip-grad 0.5 \
                --evaluate-freq 2 \
                --opt adafactor \
                --dist-eval \
                --drop 0.5 \
                --drop-path 0.5 \
                --drop-block 0.5 \
                # --data-path /userhome/traffic_data/pclab/workspace/workspace/huayuanxiao/data_hyx/dataSet/21K \
                # --lr  5e-5 \
                # --sync-bn \
                # --resume output/best_model.pth \
                # --eval
